from PyQt6.QtWidgets import QDialog, QDialogButtonBox
from PyQt6.QtGui import QPixmap, QImage, QTransform, QIntValidator, QIcon
from PyQt6.QtCore import Qt, QLocale
from PyQt6.uic import loadUi
from logger import logger
from resourcePath import resourcePath
import qtawesome as qta
from changeIconTheme import changeIconTheme
from extract import extract_image

# --------------------------------------------------   
# Classe pour la boîte de dialogue d'importation
# -------------------------------------------------- 

class importDialog(QDialog):

    def __init__(self,first_frame, last_frame,camera_Width,camera_Height ,fps,frame_count,duration, theme, video_file, parent=None):
        super().__init__(parent)
        logger.info("Affichage de ImportDialog")

        self.theme = theme

        # Set the attributes
        self.first_frame = first_frame
        self.last_frame = last_frame
        self.duration = duration
        self.fps = fps
        self.frame_count = frame_count
        self.frame_count_ref = frame_count
        self.first_frame_index = 1
        self.last_frame_index = frame_count
        self.camera_Width = camera_Width
        self.camera_Height = camera_Height
        self.video_file = video_file

        self.newframe_count = self.frame_count
        self.newcamera_Width = self.camera_Width
        self.newcamera_Height = self.camera_Height

        # Load the UI and configure it
        logger.info("Chargement de import.ui")
        loadUi(resourcePath('assets/ui/import.ui'), self)
        self.setWindowIcon(QIcon(resourcePath('assets/icons/icon.png')))
        self.setWindowTitle("Importation d'une vidéo")

        
        self.paramBox.setStyleSheet("QGroupBox#paramBox {border:0;}")
        self.paramButton.clicked.connect(self.open_param)

        self.right_btn.setIcon(qta.icon('fa5s.redo', color='white'))
        self.left_btn.setIcon(qta.icon('fa5s.undo', color='white'))

        self.first_img = QImage(first_frame, first_frame.shape[1], first_frame.shape[0], first_frame.shape[1] * 3,QImage.Format.Format_RGB888)
        pixmap = QPixmap(self.first_img).scaled(200, 200,Qt.AspectRatioMode.KeepAspectRatio, Qt.TransformationMode.FastTransformation)
        self.img_label.setPixmap(pixmap)

        self.last_img = QImage(last_frame, last_frame.shape[1], last_frame.shape[0], last_frame.shape[1] * 3,QImage.Format.Format_RGB888)
        pixmap = QPixmap(self.last_img).scaled(200, 200,Qt.AspectRatioMode.KeepAspectRatio, Qt.TransformationMode.FastTransformation)
        self.img_label2.setPixmap(pixmap)

        self.rotation = 0

        self.left_btn.clicked.connect(lambda : self.rotate(-90))
        self.right_btn.clicked.connect(lambda : self.rotate(90))

        self.first_image_index.setMaximum(frame_count)
        self.first_image_index.setValue(1)
        self.last_image_index.setMaximum(frame_count)
        self.last_image_index.setValue(frame_count)

        self.first_image_index.setMinimum(1)
        self.last_image_index.setMinimum(2)

        self.largeur_ref.setText(str(camera_Width))
        self.hauteur_ref.setText(str(camera_Height))
        self.images_ref.setText(str(self.frame_count))
        self.duree_ref.setText(str(round(self.duration,2)))
        self.duree_perso.setText(str(round(self.duration,2)))
        self.fps_ref.setText(str(round(fps,2)))
        self.fps_perso.setText(str(round(fps,2)))
        self.orientation_ref.setText("0°")
        self.orientation_perso.setText("0°")

        self.onlyInt = QIntValidator()
        self.onlyInt .setLocale(QLocale("en_US"))
        self.largeur_perso.setValidator(self.onlyInt)
        self.hauteur_perso.setValidator(self.onlyInt)
        self.images_perso.setValidator(self.onlyInt)

        self.largeur_perso.setText(str(camera_Width))
        self.hauteur_perso.setText(str(camera_Height))
        self.images_perso.setText(str(self.frame_count))

        # Add event listeners
        self.first_image_index.valueChanged.connect(self.update_length)
        self.last_image_index.valueChanged.connect(self.update_length)

        self.images_perso.textChanged.connect(self.calculate_fps)
        self.largeur_perso.textChanged.connect(self.largeur_test)
        self.hauteur_perso.textChanged.connect(self.hauteur_test)

        self.btn_apply = self.buttonBox.button(QDialogButtonBox.StandardButton.Ok)

        if self.frame_count >= 150:
            self.images_perso.setStyleSheet("border: 1px solid red;")
            self.first_image_index.setStyleSheet("border: 1px solid red;")
            self.last_image_index.setStyleSheet("border: 1px solid red;")
            self.autoRatio.setStyleSheet("color:red;")
            # self.message.show()
            # self.message.setText("Le nombre d'image doit être inférieur à 150 !")
            self.newframe_count = 0
        # else :
        #     self.message.hide()

        if self.frame_count < 30:
            self.autoRatio.setEnabled(False)
        else :
            self.autoRatio.setEnabled(True)

        # Connect the autoRatio checkbox
        self.autoRatio.stateChanged.connect(self.auto_ratio)

        # Add the tooltip for images_perso
        self.infoButton.setIcon(qta.icon('fa5s.info-circle', color='white'))
        self.infoButton.setStyleSheet("border: 0;")
        
        self.update_ratio()

        
        self.paramShow = True
        self.open_param()

        self.check_state()

        self.paramButton.setIcon(qta.icon('fa5s.arrow-right', color='white'))
        self.paramButton.setStyleSheet("border: 0;text-align: left;")
        changeIconTheme(self.theme, self)

    def open_param(self):
        logger.info("Ouverture/Fermeture de la boîte de paramètres de la fenêtre d'importation")
        if self.paramShow == False:
            self.paramBox.show()
            self.paramShow = True
            self.setMinimumSize(425, 630)
            self.setMaximumSize(425, 630)
            self.resize(425,630)
            self.paramButton.setText("Afficher moins d'options")
            self.paramButton.setIcon(qta.icon('fa5s.arrow-down', color='white'))
            changeIconTheme(self.theme, self)
        else:
            self.paramBox.hide()
            self.paramShow = False
            self.setMinimumSize(425,360)
            self.setMaximumSize(425,360)
            self.resize(425,360)
            self.paramButton.setText("Afficher plus d'options")
            self.paramButton.setIcon(qta.icon('fa5s.arrow-right', color='white'))
            changeIconTheme(self.theme, self)

    def calculate_fps(self, string, duration = None):
        # Check if the string is a number and if it is not empty
        if string.isnumeric():
            if string == '' or int(string) > self.frame_count or int(string) > 150:
                self.fps_perso.setText("-")
                self.images_perso.setStyleSheet("border: 1px solid red;")
                self.first_image_index.setStyleSheet("border: 1px solid red;")
                self.last_image_index.setStyleSheet("border: 1px solid red;")
                self.autoRatio.setStyleSheet("color:red;")
                # self.message.show()
                # self.message.setText("Le nombre d'image doit être inférieur à 150 !")
                self.newframe_count = 0
            else :
                if duration == None:
                    duration = self.duration
                else :
                    duration = float(duration)
                
                self.fps = round(int(string)/duration,2)
                self.fps_perso.setText(str(self.fps))
                self.images_perso.setStyleSheet("")
                self.first_image_index.setStyleSheet("")
                self.last_image_index.setStyleSheet("")
                self.autoRatio.setStyleSheet("")
                # self.message.hide()
                self.newframe_count = int(string)

            self.update_ratio()
            self.check_state()

    def update_ratio(self):
        # Update the ratio
        if self.images_perso.text() != '' and int(self.images_perso.text()) != 0:
            ratio = round((int(self.frame_count) / int(self.images_perso.text())),1)
            time_increment = round(1/self.fps,2)*1000
            self.infoLabel.setText("Ratio : 1/"+str(ratio))
            self.infoButton.setToolTip("Pour avoir "+self.images_perso.text()+" images en tout,\n le logiciel extraira 1 image\n sur "+str(ratio)+" soit une image toutes\n les "+str(time_increment)+" ms.")
        
    def auto_ratio(self):
        # If autoRatio is checked, set the ratio to 30
        if self.autoRatio.isChecked():
            # Set the images_perso value to 30
            if self.frame_count >= 30:
                self.images_perso.setText("30")
                self.newframe_count = 30
                # Calculate the fps
                self.calculate_fps("30")
        else:
            # Set the images_perso value to the frame_count
            self.images_perso.setText(str(self.frame_count))
            # Calculate the fps
            self.calculate_fps(str(self.frame_count))

    def largeur_test(self, string):
        if string == '' or int(string) > self.camera_Width:
            self.largeur_perso.setStyleSheet("border: 1px solid red;")
            self.newcamera_Width = 0
        else :
            self.largeur_perso.setStyleSheet("")
            self.newcamera_Width = int(string)
        self.check_state()
    
    def hauteur_test(self, string):
        if string == '' or int(string) > self.camera_Height:
            self.hauteur_perso.setStyleSheet("border: 1px solid red;")
            self.newcamera_Height = 0
        else :
            self.hauteur_perso.setStyleSheet("")
            self.newcamera_Height = int(string)
        self.check_state()

    def check_state(self):
        if self.newcamera_Height != 0 and self.newcamera_Width  != 0 and self.newframe_count != 0:
            self.btn_apply.setEnabled(True)
        else :
            self.btn_apply.setEnabled(False)

    def rotate(self, rot):
        my_transform = QTransform()
        my_transform.rotate(rot)
        self.first_img = self.first_img.transformed(my_transform)
        pixmap = QPixmap(self.first_img).scaled(200, 200,Qt.AspectRatioMode.KeepAspectRatio,  Qt.TransformationMode.FastTransformation)
        self.img_label.setPixmap(pixmap)

        self.last_img = self.last_img.transformed(my_transform)
        pixmap = QPixmap(self.last_img).scaled(200, 200,Qt.AspectRatioMode.KeepAspectRatio,  Qt.TransformationMode.FastTransformation)
        self.img_label2.setPixmap(pixmap)

        self.rotation += rot
        if self.rotation == 270:
            self.rotation = -90
        elif self.rotation == 360 or self.rotation == -360:
            self.rotation = 0
        elif self.rotation == -180:
            self.rotation = 180
        elif self.rotation == -270:
            self.rotation = 90

        self.orientation_perso.setText(str(self.rotation)+"°")

    def GetValue(self):
        logger.info("Envoie des valeurs choisies lors de l'importation")
        if self.newcamera_Height != 0 and self.newcamera_Width  != 0 and self.newframe_count != 0:
            return (self.newcamera_Width,self.newcamera_Height, self.newframe_count, self.rotation, self.first_frame_index-1, self.last_frame_index-1)

    def update_length(self):
        if (self.first_image_index.text() != 0 and self.last_image_index.text() != 0):
            # Get the first and last frame index
            self.first_frame_index = int(self.first_image_index.text())
            self.last_frame_index = int(self.last_image_index.text())

            # Set the min/max value of the last_image_index spin box
            self.first_image_index.setMinimum(1)
            self.first_image_index.setMaximum(self.last_frame_index-1)
            self.last_image_index.setMinimum(self.first_frame_index+1)
            self.last_image_index.setMaximum(self.frame_count_ref)

            # Get the images
            first_frame = extract_image(self.video_file, self.first_frame_index-1)
            last_frame = extract_image(self.video_file, self.last_frame_index-1)

            self.first_img = QImage(first_frame, first_frame.shape[1], first_frame.shape[0], first_frame.shape[1] * 3,QImage.Format.Format_RGB888)
            pixmap = QPixmap(self.first_img).scaled(200, 200,Qt.AspectRatioMode.KeepAspectRatio, Qt.TransformationMode.FastTransformation)
            self.img_label.setPixmap(pixmap)

            self.last_img = QImage(last_frame, last_frame.shape[1], last_frame.shape[0], last_frame.shape[1] * 3,QImage.Format.Format_RGB888)
            pixmap = QPixmap(self.last_img).scaled(200, 200,Qt.AspectRatioMode.KeepAspectRatio, Qt.TransformationMode.FastTransformation)
            self.img_label2.setPixmap(pixmap)

            if self.last_frame_index-self.first_frame_index+1 < 30:
                self.autoRatio.setEnabled(False)
            else:
                self.autoRatio.setEnabled(True)

            if self.autoRatio.isChecked() == False :
                # Update "images_ref" and "images_perso" values
                self.images_perso.setText(str(self.last_frame_index-self.first_frame_index+1))
            else:
                self.images_perso.setText("30")

            # Update the duration value
            self.duration = (self.last_frame_index-self.first_frame_index+1)/float(self.fps_ref.text())
            self.duree_perso.setText(str(round(self.duration,2)))

            self.frame_count = self.last_frame_index-self.first_frame_index+1

            # Update the fps value
            self.calculate_fps(str(self.last_frame_index-self.first_frame_index+1), self.duration)