from logger import logger
from PyQt6.QtWidgets import QDialog, QDialogButtonBox, QVBoxLayout, QHBoxLayout, QLabel, QWidget
from PyQt6.QtGui import QIcon
from PyQt6.QtCore import Qt
from changeIconTheme import changeIconTheme
from resourcePath import resourcePath

# --------------------------------------------------   
# Classe pour les boîtes de dialogue simples
# -------------------------------------------------- 
class messageDialog(QDialog):

    def __init__(self, themessage, theme):
        super().__init__()

        logger.info("Affichage de CustomDialog")

        self.setWindowTitle("Message")
        self.setWindowIcon(QIcon(resourcePath('assets/icons/icon.png')))
        
        QBtn = QDialogButtonBox.StandardButton.Ok | QDialogButtonBox.StandardButton.Cancel

        self.buttonBox = QDialogButtonBox(QBtn)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        self.layout = QVBoxLayout()
        self.mainlayout = QHBoxLayout()
        self.mainwidget = QWidget()

        message = QLabel(themessage)
        message.setTextFormat(Qt.TextFormat.RichText)
        message.setOpenExternalLinks(True)
        
        self.mainlayout.addWidget(message)
        self.mainwidget.setLayout(self.mainlayout)
        self.layout.addWidget(self.mainwidget)
        self.layout.addWidget(self.buttonBox)
        
        self.setLayout(self.layout)

        changeIconTheme(theme, self)


    def stop(self):
        logger.info("Fermeture de CustomDialog")
        self.done(0)