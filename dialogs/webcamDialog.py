# --------------------------------------------------
# webcamDialog.py
# This file contains the webcamDialog class which is used to handle the webcam dialog.
# --------------------------------------------------

from PyQt6.QtWidgets import QDialog, QDialogButtonBox, QGraphicsPixmapItem, QGraphicsScene
from PyQt6.QtGui import QPixmap, QImage, QIntValidator, QIcon
from PyQt6.QtCore import Qt, QLocale, QTimer, QThread
from PyQt6.uic import loadUi
from logger import logger
from resourcePath import resourcePath
from openFolder import openFolder
import os
import sys
from webcam import (webcam_init, webcam_get_image,list_webcam_ports, release_cap, set_property, set_exposition)
import qtawesome as qta
from changeIconTheme import changeIconTheme
from workers.webcamWorker import webcamWorker

# --------------------------------------------------   
# Classe pour la boîte de dialogue de la webcam
# -------------------------------------------------- 

class webcamDialog(QDialog):

    # Init the webcam dialog
    def __init__(self, application_path, theme):
        super().__init__()
        logger.info("Affichage de WebcamDialog")

        # Load the UI
        loadUi(resourcePath('assets/ui/webcam.ui'), self)
        # Set the window icon and title
        self.setWindowIcon(QIcon(resourcePath('assets/icons/icon.png')))
        self.setWindowTitle("Enregistrement d'une vidéo")

        # Set the theme
        self.theme = theme

        # Set the "OK" and cancel buttons behavior
        self.btn_apply = self.buttonBox.button(QDialogButtonBox.StandardButton.Ok)
        self.btn_apply.clicked.connect(self.importVideo)
        self.btn_apply.setEnabled(False)  
        changeIconTheme(self.theme, self)

        self.btn_cancel = self.buttonBox.button(QDialogButtonBox.StandardButton.Cancel)
        self.OK = False
        self.btn_cancel.clicked.connect(self.close)

        # Set the icons and the tooltips
        self.capture.setIcon(qta.icon('fa5s.video', color='white'))
        self.capture.setProperty('class', 'greenBtn')

        self.refreshButton.setIcon(qta.icon('fa5s.undo', color='white'))
        self.paramButton.setIcon(qta.icon('fa5s.cog', color='white'))
        
        self.infos.setText("<p><b>"+str(os.path.join(application_path ,  "videos", ""))+"</b></p>")
        self.infos.setTextInteractionFlags(Qt.TextInteractionFlag.TextSelectableByMouse)
        
        self.openFolderButton.clicked.connect(lambda : openFolder(application_path))
        self.openFolderButton.setIcon(qta.icon('fa5s.folder-open', color='white'))

        self.applyButton.setIcon(qta.icon('fa5s.check', color='white'))

        self.defaultButton.setIcon(qta.icon('fa5s.undo', color='white'))

        self.questionFps.setIcon(qta.icon('fa5s.question-circle', color='white'))
        self.questionExposition.setIcon(qta.icon('fa5s.question-circle', color='white'))
        self.questionFps.setToolTip("Permet de caper le nombre d\'images par seconde.\n Si la valeur réelle du nombre d\'images par seconde\n est inférieure, ce paramètre sera ignoré. 0 signifie\n que la webcam utilisera le maximum possible.")
        self.questionExposition.setToolTip('La diminution du temps d\'exposition permet\n d\'éviter le flou cinétique mais nécessite un\n éclairage suffisant.')

        # Init the variables
        self.camera_id = None
        self.recordDone = False
        self.firstStart = True
        self.cap = None 
        self.webcam_params = {}
        self.paramShow = False

        # Hide the parameters box and resize the dialog
        self.paramBox.hide()
        self.setMinimumSize(420, 600)
        self.setMaximumSize(420, 600)
        self.resize(420, 600)
        self.paramBox.setStyleSheet("QGroupBox#paramBox {border:0;}")
        
        # Connect the buttons to the functions
        self.refreshButton.clicked.connect(self.refresh)
        self.paramButton.clicked.connect(self.open_param)
        self.selectWebcam.activated.connect(self.changeCamera)
        self.defaultButton.clicked.connect(self.reset_params)

        self.ExpositionCheckBox.setChecked(True)
        self.ExpositionCheckBox.stateChanged.connect(lambda : self.change_exposition(self.expositionEdit.text()))

        self.capture.clicked.connect(lambda : self.capture_start(application_path))

        # Init the validators
        self.onlyInt = QIntValidator()
        self.onlyInt .setLocale(QLocale("en_US"))
        
        self.heightEdit.setValidator(self.onlyInt)
        self.widthEdit.setValidator(self.onlyInt)
        self.expositionEdit.setValidator(self.onlyInt)
        self.fpsEdit.setValidator(self.onlyInt)

        # Get the graphics view and set the scene
        self.scene = QGraphicsScene()
        self.graphicsView.setScene(self.scene)
        self.scenePixmapItem = None

        # Reset the webcam parameters
        self.refresh()

        self.widthEdit.setText(str(int(self.webcam_params["res_width"])))
        self.heightEdit.setText(str(int(self.webcam_params["res_height"])))
        if sys.platform == "win32":
            logger.info("L'exposition choisie est : " + str(self.exposition))
            self.expositionEdit.setText(str(self.webcam_params["exposition"]))
        else:
            self.expositionEdit.setText(str(self.webcam_params["exposition"]))

        # Update the buttons icons
        changeIconTheme(self.theme, self)

    # Update the parameters
    def update_params(self):
        self.luminositeSlider.setRange(0, 255)
        self.luminositeSlider.setValue(self.webcam_params["luminosite"])
        self.luminositeSlider.valueChanged.connect(self.change_luminosite)
        self.luminositeEdit.setValidator(self.onlyInt)
        self.luminositeEdit.setText(str(self.webcam_params["luminosite"]))
        self.luminositeEdit.textChanged.connect(self.change_luminosite)

        self.contrasteSlider.setRange(0, 255)
        self.contrasteSlider.setValue(self.webcam_params["contraste"])
        self.contrasteSlider.valueChanged.connect(self.change_contraste)
        self.contrasteEdit.setValidator(self.onlyInt)
        self.contrasteEdit.setText(str(self.webcam_params["contraste"]))
        self.contrasteEdit.textChanged.connect(self.change_contraste)

        self.expositionEdit.setValidator(self.onlyInt)
        self.expositionEdit.setText(str(self.webcam_params["exposition"]))
        self.expositionEdit.textChanged.connect(self.change_exposition)
        self.expositionEdit.setEnabled(False)

        self.saturationSlider.setRange(0, 255)
        self.saturationSlider.setValue(self.webcam_params["saturation"])
        self.saturationSlider.valueChanged.connect(self.change_saturation)
        self.saturationEdit.setValidator(self.onlyInt)
        self.saturationEdit.setText(str(self.webcam_params["saturation"]))
        self.saturationEdit.textChanged.connect(self.change_saturation)

        self.gammaSlider.setRange(0, 255)
        self.gammaSlider.setValue(self.webcam_params["gamma"])
        self.gammaSlider.valueChanged.connect(self.change_gamma)
        self.gammaEdit.setValidator(self.onlyInt)
        self.gammaEdit.setText(str(self.webcam_params["gamma"]))
        self.gammaEdit.textChanged.connect(self.change_gamma)

        self.netteteSlider.setRange(0, 255)
        self.netteteSlider.setValue(self.webcam_params["nettete"])
        self.netteteSlider.valueChanged.connect(self.change_nettete)
        self.netteteEdit.setValidator(self.onlyInt)
        self.netteteEdit.setText(str(self.webcam_params["nettete"]))
        self.netteteEdit.textChanged.connect(self.change_nettete)

        self.blancSlider.setRange(2000, 6500)
        self.blancSlider.setValue(self.webcam_params["blanc"])
        self.blancSlider.valueChanged.connect(self.change_blanc)
        self.blancEdit.setValidator(self.onlyInt)
        self.blancEdit.setText(str(self.webcam_params["blanc"]))
        self.blancEdit.textChanged.connect(self.change_blanc)

        self.teinteSlider.setRange(0, 255)
        self.teinteSlider.setValue(self.webcam_params["teinte"])
        self.teinteSlider.valueChanged.connect(self.change_teinte)
        self.teinteEdit.setValidator(self.onlyInt)
        self.teinteEdit.setText(str(self.webcam_params["teinte"]))
        self.teinteEdit.textChanged.connect(self.change_teinte)

        self.widthEdit.setValidator(self.onlyInt)
        self.widthEdit.setText(str(int(self.webcam_params["res_width"])))
        self.heightEdit.setValidator(self.onlyInt)
        self.heightEdit.setText(str(int(self.webcam_params["res_height"])))
        self.applyButton.clicked.connect(self.apply)

        self.fpsEdit.setValidator(self.onlyInt)
        self.fpsEdit.setText(str(int(self.webcam_params["fps"])))
        self.fpsEdit.textChanged.connect(self.change_fps)

    def change_fps(self, value):
        if value != '':
            self.webcam_params["fps"]=int(value)
            self.fps_max = int(value)

    def change_luminosite(self, value):
        #logger.info("Modification de la luminosité : " + str(value))
        if value != '':
            self.webcam_params["luminosite"]=int(value)
            self.luminositeSlider.setValue(int(value))
            self.luminositeEdit.setText(str(value))
            self.cap = set_property("luminosite", int(value), self.cap)

    def change_contraste(self, value):
        #logger.info("Modification du contraste : " + str(value))
        if value != '':
            self.webcam_params["contraste"]=int(value)
            self.contrasteSlider.setValue(int(value))
            self.contrasteEdit.setText(str(value))
            self.cap = set_property("contraste", int(value), self.cap)

    def change_exposition(self, value):
        #logger.info("Modification de l'exposition : " + str(value))
        if self.ExpositionCheckBox.isChecked() == False:
            self.expositionEdit.setEnabled(True)
        else:
            self.expositionEdit.setEnabled(False)
        if value != '':
            self.webcam_params["exposition"]=int(value)
            self.cap = set_exposition(int(value), self.ExpositionCheckBox.isChecked(), self.cap)

    def change_saturation(self, value):
        #logger.info("Modification de la saturation : " + str(value))
        if value != '':
            self.webcam_params["saturation"]=int(value)
            self.saturationSlider.setValue(int(value))
            self.saturationEdit.setText(str(value))
            self.cap = set_property("saturation", int(value), self.cap)

    def change_gamma(self, value):
        #logger.info("Modification du gamma : " + str(value))
        if value != '':
            self.webcam_params["gamma"]=int(value)
            self.gammaSlider.setValue(int(value))
            self.gammaEdit.setText(str(value))
            self.cap = set_property("gamma", int(value), self.cap)

    def change_nettete(self, value):
        #logger.info("Modification de la nettete : " + str(value))
        if value != '':
            self.webcam_params["nettete"]=int(value)
            self.netteteSlider.setValue(int(value))
            self.netteteEdit.setText(str(value))
            self.cap = set_property("nettete", int(value), self.cap)

    def change_blanc(self, value):
        #logger.info("Modification de la balance des blancs : " + str(value))
        if value != '':
            self.webcam_params["nettete"]=int(value)
            self.blancSlider.setValue(int(value))
            self.blancEdit.setText(str(value))
            self.cap = set_property("blanc", int(value), self.cap)

    def change_teinte(self, value):
        #logger.info("Modification de la teinte : " + str(value))
        if value != '':
            self.webcam_params["teinte"]=int(value)
            self.teinteSlider.setValue(int(value))
            self.teinteEdit.setText(str(value))
            self.cap = set_property("teinte", int(value), self.cap)

    # Reset the webcam parameters to the default ones
    def reset_params(self):
        logger.info("Restoration des paramètres par défaut de la caméra")
        if self.cap != None and self.firstStart != True:
            # Release the webcam
            release_cap(self.cap)
            # Stop the timer to update the frame
            self.timer.stop()
            self.cap = None 
        
        # Apply the default parameters (as first start)
        self.firstStart = True
        self.apply()            

    # Show/hide the parameters box by resizing the dialog
    def open_param(self):
        logger.info("Affichage des paramètres de la webcam")
        if self.paramShow == False:
            self.paramBox.show()
            self.paramShow = True
            self.setMinimumSize(840, 600)
            self.setMaximumSize(840, 600)
            self.resize(840, 600)
        else:
            self.paramBox.hide()
            self.paramShow = False
            self.setMinimumSize(420, 600)
            self.setMaximumSize(420, 600)
            self.resize(420, 600)

    # Refresh the webcam ports
    def refresh(self):
        logger.info("Recherche des ports pour les webcams")
        if self.cap != None and self.firstStart != True:
            release_cap(self.cap)
            self.timer.stop()
            self.cap = None 

        # Clear the webcam list
        self.selectWebcam.clear()
        # Get the webcam ports
        camera_ports = list_webcam_ports()
        logger.info("Webcams trouvées aux ports : "+str(camera_ports))
        if len(camera_ports) != 0:
            # Add the webcam ports to the list
            for i in range(len(camera_ports)):
                self.selectWebcam.addItem("Camera " + str(camera_ports[i]))
                
            # Set the first webcam as the default one
            self.camera_id = camera_ports[0]
            # Enable the capture button
            self.capture.setEnabled(True)
            changeIconTheme(self.theme, self)
        else:
            self.capture.setEnabled(False)
            changeIconTheme(self.theme, self)

        # Apply the webcam parameters
        self.apply()

    # Apply the webcam parameters
    def apply(self):
        logger.info("Application des paramètres")
        # If self.cap already exists and the webcam is not started for the first time, release the webcam
        if self.cap != None and self.firstStart != True:
            release_cap(self.cap)
            self.timer.stop()
            self.cap = None 
        
        # Init the variables
        self.captureStatus = False    
        self.image = None 
        self.ret = False

        # Create the timer to update the frame
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_frame)

        if self.cap is None:
            # If the webcam is started for the first time, initialize it
            if self.firstStart:
                logger.info("Première initialisation de la webcam")
                self.cap, self.webcam_params = webcam_init(self.camera_id)
                # Get the webcam parameters
                self.fps_max, self.res_width, self.res_height,self.exposition = self.webcam_params["fps"],self.webcam_params["res_width"],self.webcam_params["res_height"],self.webcam_params["exposition"]
                self.default_webcam_params = self.webcam_params

                # Hide the image_label
                self.image_label.hide()

                # Update the parameters
                self.update_params()
            else:
                logger.info("Initialisation de la webcam avec les paramètres personnalisés")
                # Apply the new parameters
                self.webcam_params["res_width"] = int(self.widthEdit.text())
                self.webcam_params["res_height"] = int(self.heightEdit.text())
                self.webcam_params["fps"] = int(self.fpsEdit.text())
                logger.info("Nouveaux paramètres de la webcam : "+str(self.webcam_params))

                # Initialize the webcam with the new parameters
                self.cap, self.webcam_params = webcam_init(self.camera_id,self.webcam_params)
                self.fps_max, self.res_width, self.res_height,self.exposition = self.webcam_params["fps"],self.webcam_params["res_width"],self.webcam_params["res_height"],self.webcam_params["exposition"]

                self.widthEdit.setText(str(int(self.webcam_params["res_width"])))
                self.heightEdit.setText(str(int(self.webcam_params["res_height"])))
                self.fpsEdit.setText(str(int(self.webcam_params["fps"])))

                # Hide the image_label
                self.image_label.hide()

        logger.info("Démarrage du timer pour le rafraichissement de l'affichage")
        # Start the timer to update the frame
        self.timer.start()
        # Set the first start to False
        self.firstStart = False

    # Change the camera
    def changeCamera(self):
        release_cap(self.cap)
        self.camera_id = int(self.selectWebcam.currentText().replace('Camera ', ''))
        self.apply()

    # Update the frame (if the capture is started, also capture the image)
    def update_frame(self):
        if self.cap != None:
            self.ret, self.image = webcam_get_image(self.cap)
            if self.ret:
                self.displayImage(self.image)
                if self.captureStatus == True:
                    self.capture_image()

    # Handle the start/end of the capture
    def capture_start(self, application_path):
        # If the capture is already started, stop it
        if self.captureStatus == True :
            logger.info("Début du traitement de la vidéo")
            self.capture.setEnabled(False)
            self.capture.setText("Traitement de la vidéo")
            self.capture.setStyleSheet("font-weight: bold;background-color: '#EB8B4B';border: 1px solid #EB8B4B;color: '#fff';")
            
            # Send a stop signal to the worker
            self.webcam_worker.finished.emit()
        # If the capture is not started, start it
        else:
            logger.info("Démarrage de l'enregistrement")
            # Try to create the QThread object
            try :
                # Create the thread
                logger.info("Création du thread pour l'enregistrement de la webcam")
                self.webcam_thread = QThread()
                self.webcam_thread.setTerminationEnabled(True)

                # Create the worker
                self.webcam_worker = webcamWorker(application_path, self.res_width, self.res_height, self.fps_max)

                # Move the worker to the thread
                self.webcam_worker.moveToThread(self.webcam_thread)

                # Connect the signals : the worker is started when the thread is started and the data is sent to the main thread through the get_webcam_end_data function
                self.webcam_thread.started.connect(self.webcam_worker.run)
                self.webcam_worker.data.connect(self.get_webcam_end_data)

                # Start the thread
                self.webcam_thread.start()

                self.captureStatus = True
                self.capture.setText("Arrêter l'enregistrement")
                self.capture.setStyleSheet("font-weight: bold;background-color: '#EE3333';border: 1px solid #FF3e3e;color: '#fff';")
                self.btn_apply.setEnabled(False)
                changeIconTheme(self.theme, self)
                logger.info("Démarrage du timer pour l'enregistrement")
            # If an error occurs, the exception is caught
            except Exception as ex:
                logger.exception("Une erreur est survenue : " + str(ex))

    # Get the data from the worker
    def get_webcam_end_data(self, fps, seconds, video_path):
        logger.info("Durée de la vidéo enregistrée: "+str(seconds))
        logger.info("Nombre d'images par seconde estimé de la vidéo enregistrée : "+str(fps))        
        logger.info("Fin de l'enregistrement vidéo")
        
        # Set the video path
        self.video_path = video_path

        # Update the capture button status
        self.capture.setEnabled(True)
        self.capture.setText("Démarrer l'enregistrement")
        self.capture.setStyleSheet("font-weight: bold;background-color: '#1b7a46';border: 1px solid #2b8a56;color: '#fff';")
        logger.info("Arrêt du timer pour l'enregistrement")

        # Set the record done status and enable the apply button
        self.recordDone = True
        self.captureStatus = False
        self.btn_apply.setEnabled(True)
        changeIconTheme(self.theme, self)

        # Stop the thread
        logger.info("Arrêt du thread webcam")
        self.webcam_thread.quit()

    # Capture the image (send the image to the worker with the signal image_signal)
    def capture_image(self):
        if self.captureStatus == True and self.ret == True:
            #QtWidgets.QApplication.beep()
            self.webcam_worker.image_signal.emit(self.image)

    # Display the image in the graphics view
    def displayImage(self, img):
        qformat = QImage.Format.Format_Indexed8
        if len(img.shape)==3 :
            if img.shape[2]==4:
                qformat = QImage.Format.Format_RGBA8888
            else:
                qformat = QImage.Format.Format_RGB888
        outImage = QImage(img, img.shape[1], img.shape[0], img.strides[0], qformat)
        outImage = outImage.rgbSwapped()
        pixmap = QPixmap.fromImage(outImage)

        if self.scenePixmapItem is None:
            self.scenePixmapItem = QGraphicsPixmapItem()
            self.scene.addItem(self.scenePixmapItem)
            self.scenePixmapItem.setZValue(0)
            self.fitInView(self.scene.sceneRect(), Qt.AspectRatioMode.KeepAspectRatio)
        else : 
            self.scenePixmapItem.setPixmap(pixmap)
            self.fitInView(self.scenePixmapItem, Qt.AspectRatioMode.KeepAspectRatio)

    # Fit the image in the graphics view
    def fitInView(self, rect, aspectRatioMode):
        self.graphicsView.fitInView(rect, aspectRatioMode)

    # Release the webcam
    def release(self):
        release_cap(self.cap)

    # Handle the "OK" button to import the video
    def importVideo(self):
        self.OK = True
        self.close()
