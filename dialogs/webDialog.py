import os
import qrcode
from PyQt6.QtCore import Qt
from PyQt6.QtGui import QPixmap, QImage, QIcon
from PyQt6.QtWidgets import QDialog
from PyQt6.uic import loadUi
from webserver import get_address
from logger import logger
from openFolder import openFolder
from resourcePath import resourcePath
import qtawesome as qta
from changeIconTheme import changeIconTheme

# --------------------------------------------------   
# Classe pour les boîtes de dialogue simples
# -------------------------------------------------- 
class webDialog(QDialog):

    def __init__(self, application_path, theme):
        super().__init__()
        logger.info("Affichage de WebDialog")

        logger.info("Chargement de webserver.ui")
        loadUi(resourcePath('assets/ui/webserver.ui'), self)
        self.setWindowIcon(QIcon(resourcePath('assets/icons/icon.png')))
        self.setWindowTitle("Importation depuis un smartphone")
        
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        address = get_address()

        self.instructions = "<p style=\"font-size: 16px;\"><ul><li>Connecter le périphérique (smartphone, tablette...) sur le même réseau (wifi ou partage de connexion) que l'ordinateur executant ChronoPhys.</li><li>Scanner le QRCode suivant à l'aide du périphérique ou entrer l'adresse suivante dans le navigateur : <b>http://"+address+":8080<b></li><li>Suivre les instructions sur le périphérique.</li></ul><hr>Vous retrouverez les fichiers videos ici : <b>"+str(os.path.join(application_path ,  "videos", ""))+"</b></p>"
        self.message.setText(self.instructions)

        self.message.setTextFormat(Qt.TextFormat.RichText)
        self.message.setTextInteractionFlags(Qt.TextInteractionFlag.TextSelectableByMouse)
        self.message.setStyleSheet("padding :15px")
      
        self.message.setWordWrap(True)
        pixmap = self.pil2pixmap(self.generate_qr()).scaled(150, 150, Qt.AspectRatioMode.KeepAspectRatio)
        self.label_qr.setPixmap(pixmap) 
        self.label_qr.resize(150,150) 

        self.openFolderButton.clicked.connect(lambda : openFolder(application_path))
        self.openFolderButton.setIcon(qta.icon('fa5s.folder-open', color="white"))

        changeIconTheme(theme, self)

    def generate_qr(self):
        input_data = "http://"+get_address()+":8080"
        qr = qrcode.QRCode(version=1, box_size=20, border=2)
        qr.add_data(input_data)
        qr.make(fit=True)
        img = qr.make_image(fill='black', back_color='white')
        return img

    def pil2pixmap(self, im):
        im2 = im.convert("RGBA")
        data = im2.tobytes("raw", "RGBA")
        qim = QImage(data, im.size[0], im.size[1], QImage.Format.Format_ARGB32)
        pixmap = QPixmap.fromImage(qim)
        return pixmap

    def stop(self):
        logger.info("Fermeture de WebDialog")
        self.done(0)