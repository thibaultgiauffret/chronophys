# --------------------------------------------------
# webcamWorker.py
# This file contains the webcamWorker class which is used to handle the webcam recording.
# --------------------------------------------------

from PyQt6.QtCore import QObject, pyqtSignal, QThread
from logger import logger
import time
from cv2 import (
    VideoCapture,
    VideoWriter,
    VideoWriter_fourcc
)
import os
import time
from datetime import datetime
import numpy as np

# --------------------------------------------------
# ProcessingThread class to process the video after webcam recording
# --------------------------------------------------
class ProcessingThread(QThread):
    def __init__(self, worker):
        super().__init__()
        self.worker = worker

    def run(self):
        self.worker.process_video()

# --------------------------------------------------   
# webcamWorker class to handle the webcam recording
# -------------------------------------------------- 

class webcamWorker(QObject):

    # Signals
    finished = pyqtSignal()
    data = pyqtSignal(float, float, str)
    image_signal = pyqtSignal(object)

    # Constructor
    def __init__(self, path, width, height, fps_max, parent=None):
        super(webcamWorker, self).__init__(parent)
        logger.info("Démarrage du Worker pour le traitement de la vidéo")
        # Initialize the variables
        self.path = path
        self.video_path = ""
        self.width = width
        self.height = height
        self.start = ""
        self.fps_max = fps_max
        self.image_counter = 0

        # Create the processing thread
        self.processing_thread = ProcessingThread(self)

        # Connect the image signal to the image capture function
        self.image_signal.connect(self.image_capture)

        # Connect the finished signal to the stop function
        self.finished.connect(self.stop)

    # Function to capture the image and write it to the video
    def image_capture(self, image):
        try :
            if self.out1 == None:
                return
            if self.image_counter == 0:
                self.start = time.time()
            self.out1.write(image)
            self.image_counter += 1
        except Exception as e:
            logger.error("Erreur lors de la capture de l'image : " + str(e))

    # Start the webcam capture
    # Note: The video is written with 1 fps and then reopened to set the real fps
    def run(self):
        # Get the current date and time
        now = datetime.now()
        dt_string = now.strftime("%d%m%Y_%H_%M_%S")
        # Set the fourcc codec and the path to save the video
        fourcc = VideoWriter_fourcc(*'MJPG')
        # Create the video path
        self.video_path = str(os.path.join(self.path,'videos','WebcamVid_'+dt_string+'_temp.avi'))
        # Write the video with 1 fps (video will then be reopened to set the real fps). Temporary fix ?
        try :
            self.out1 = VideoWriter(self.video_path, apiPreference = 0, fourcc = fourcc, fps = 1, frameSize =(int(self.width),int(self.height)))
            self.start = time.time()
        except Exception as e:
            logger.error("Erreur lors de la création de la vidéo : " + str(e))

    # Stop the webcam capture
    def stop(self):
        # Release the video
        self.out1.release()
        try :
            # Start the processing thread
            self.processing_thread.start()
        except Exception as e:
            logger.error("Erreur lors de l'arrêt de la capture de la vidéo : " + str(e))

    # Process the video after the capture
    def process_video(self):
        # Get the time at the end of the capture
        end = time.time()
        seconds = end - self.start

        # Calculate frames per second
        fps  = self.image_counter / seconds

        # If the fps is higher than the maximum fps, set it to the maximum fps
        if fps > self.fps_max and self.fps_max != 0:
            ratio = np.round(fps / self.fps_max)
            new_fps = fps / ratio
        else:
            new_fps = fps

        # Reopen the video to set the real fps
        path = self.video_path
        video = VideoCapture(path)
        fourcc = VideoWriter_fourcc(*'MJPG')
        # Remove "_temp" from the filename
        path = path.replace("_temp", "")
        out2 = VideoWriter(path, apiPreference = 0, fourcc = fourcc, fps = new_fps, frameSize =(int(self.width),int(self.height)))
        counter = 1
        while True:
            ret, frame = video.read()
            if ret:
                # Reduce the number of frames to match the real fps
                if fps > self.fps_max and self.fps_max != 0:
                    # Get only 1 image every "ratio" images
                    if counter % ratio == 0:
                        out2.write(frame)
                        counter=1
                    else :
                        counter += 1
                else:
                    out2.write(frame)
            else:
                break

        # Release the video
        out2.release()
        video.release()

        # Remove the temporary video
        try:
            os.remove(path.replace(".avi", "_temp.avi"))
        except Exception as e:
            logger.error("Erreur lors de la suppression de la vidéo temporaire : " + str(e))

        # Emit the fps, seconds and path
        self.data.emit(fps, seconds, path)
        logger.info("Fin du traitement de la vidéo")
        