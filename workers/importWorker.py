from PyQt6.QtCore import QObject, pyqtSignal
from logger import logger
from extract import extract_images

# --------------------------------------------------   
# Classe Worker exécutant la lecture dans un thread
# -------------------------------------------------- 

class importWorker(QObject):
    finished = pyqtSignal()
    data = pyqtSignal(list,dict,bool,list)

    def __init__(self, filename, value, parent=None):
        super(importWorker, self).__init__(parent)
        self.filename = filename
        self.value = value
        logger.info("Démarrage du Worker pour l'importation de vidéo")

    def run(self):

        self.images, self.videoConfig, error, self.video_timestamp = extract_images(str(self.filename),self.value)

        self.data.emit(self.images, self.videoConfig, error, self.video_timestamp)
        self.finished.emit()

    def stop(self):
        self.finished.emit()
        logger.info("Arrêt du Worker pour l'importation de vidéo")