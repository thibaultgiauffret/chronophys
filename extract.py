from cv2 import (
    VideoCapture,
    resize,
    CAP_PROP_FRAME_WIDTH,
    CAP_PROP_FRAME_HEIGHT,
    CAP_PROP_FRAME_COUNT,
    CAP_PROP_FPS,
    cvtColor,
    COLOR_BGR2RGB,
    CAP_PROP_POS_MSEC,
    CAP_PROP_POS_FRAMES,
    INTER_AREA,
    ROTATE_90_COUNTERCLOCKWISE,
    ROTATE_90_CLOCKWISE,
    ROTATE_180,
    rotate
)

# Return the first and last frame, the width, the height, the fps, the frame count and the duration
def extract_infos(video_file):

    error = False
    error_message = ""

    # Open the video file
    try :
        video_capture = VideoCapture(video_file)

        # Get the camera width and height
        camera_Width  = int(video_capture.get(CAP_PROP_FRAME_WIDTH))  
        camera_Height = int(video_capture.get(CAP_PROP_FRAME_HEIGHT))
        frameSize = (camera_Width, camera_Height)

        # Get the fps and the frame count
        fps = video_capture.get(CAP_PROP_FPS) 
        frame_count = int(video_capture.get(CAP_PROP_FRAME_COUNT))
        duration = frame_count/fps

        # Get the first frame
        video_capture.set(CAP_PROP_POS_FRAMES, 0) 
        ret, frameOrig = video_capture.read()
        first_frame = None
        if ret:
            first_frame = cvtColor(frameOrig, COLOR_BGR2RGB)
        else:
            error = True
            error_message = "erreur lors de la lecture de la première image"

        # Get the last frame
        video_capture.set(CAP_PROP_POS_FRAMES, frame_count-1)
        ret, frameOrig = video_capture.read()
        last_frame = None
        if ret:
            last_frame = cvtColor(frameOrig, COLOR_BGR2RGB)
        else:
            error = True
            error_message = "erreur lors de la lecture de la dernière image"
        
        return first_frame, last_frame,camera_Width,camera_Height ,fps,frame_count,duration, error, error_message
    except:
        error = True
        error_message = "erreur lors de l'ouverture de la vidéo"
        return None, None, None, None, None, None, None, error, error_message

# Return the images, the settings and an error boolean (called by the importWorker)
def extract_images(video_file,settings_perso): 
    images = []
    settings = dict()
    error = False

    # Open the video file
    video_capture = VideoCapture(video_file)

    # Get the camera width and height
    camera_Width  = int(video_capture.get(CAP_PROP_FRAME_WIDTH)) 
    camera_Height = int(video_capture.get(CAP_PROP_FRAME_HEIGHT)) 
    frameSize = (camera_Width, camera_Height)
    frameRatio= camera_Width/camera_Height
    # Apply the resolution user settings...
    if settings_perso[0] != camera_Width or settings_perso[1] != camera_Height:
        frameSize = (settings_perso[0],settings_perso[1])
        frameRatio = settings_perso[0]/settings_perso[1]
    # ... but lower the resolution if it's too high (temporary fix, set the limit in the gui ?)
    if frameSize[0] > 1280 or frameSize[1] > 720:
        frameSize = (settings_perso[0]//2,settings_perso[1]//2)

    # Get the other settings
    fps = video_capture.get(CAP_PROP_FPS) 
    first_frame = settings_perso[4]
    last_frame = settings_perso[5]
    frame_count = int(last_frame-first_frame+1)
    duration = frame_count/fps
    maxFrames = settings_perso[2]
    frameRate = 1/maxFrames*duration
    increment = round(1/(maxFrames/frame_count))
    first_frame_time = 0

    # Extract the images
    # Init the time list
    mytime = []
    if maxFrames < 151:
        # Init the counter to the first frame
        i = first_frame
        # Loop through the frames
        while video_capture.isOpened():
            # Set the frame to the current index
            video_capture.set(CAP_PROP_POS_FRAMES, i)
            # Read the frame 
            ret, frameOrig = video_capture.read()
            # If the frame is read and the index is lower or equal to the last frame
            if ret == True and i <= last_frame:
                # Get the first frame time if it's the first frame
                if i == first_frame:
                    first_frame_time = video_capture.get(CAP_PROP_POS_MSEC)
                # Get the frame time, minus the first frame time (first image is 0)
                mytime.append(video_capture.get(CAP_PROP_POS_MSEC) - first_frame_time)
                # Resize the frame and convert it to RGB
                frame = cvtColor(resize(frameOrig, frameSize,fx=0,fy=0, interpolation = INTER_AREA), COLOR_BGR2RGB)
                # Rotate the frame if needed
                if settings_perso[3] == 90:
                    frame = rotate(frame, ROTATE_90_CLOCKWISE)
                elif settings_perso[3] == -90:
                    frame = rotate(frame, ROTATE_90_COUNTERCLOCKWISE)
                elif settings_perso[3] == 180:
                    frame = rotate(frame, ROTATE_180)
                # Append the frame to the images list
                images.append(frame)
                # Increment the index
                i+=increment
            else:
                break
        
        # Set the settings
        settings["nb_images"] = len(images)
        settings["fps"] = 1/frameRate
        settings["duration"] = duration
        settings["width"] = frameSize[0]
        settings["height"] = frameSize[1]
        # Release the video capture
        video_capture.release()
        return images, settings, error, mytime
    else:
        # If the number of frames is too high, return an error
        error = True
        # Remove this ?
        settings["nb_images"] = frame_count
        settings["fps"] = 1/frameRate
        settings["duration"] = duration
        settings["width"] = frameSize[0]
        settings["height"] = frameSize[1]
        return images, settings, error, mytime

# Get an image by its index
def extract_image(video_file,i): 
    image = None
    # Open the video file
    video_capture = VideoCapture(video_file)
    # Set the frame to the current index
    video_capture.set(CAP_PROP_POS_FRAMES, i)
    # Read the frame
    ret, frameOrig = video_capture.read()
    if ret == True:
        # Resize the frame and convert it to RGB
        image = cvtColor(frameOrig, COLOR_BGR2RGB)
    # Release the video capture
    video_capture.release()
    return image
