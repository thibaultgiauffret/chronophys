from PyQt6.QtWidgets import QPushButton, QToolButton
from PyQt6.QtGui import QIcon, QPainter, QColor

def changeIconTheme(theme, window):
    for widget in window.findChildren((QPushButton, QToolButton)):
        if theme == "dark":
            icon_color = QColor(255,255,255)
        else :
            icon_color = QColor(44,44,44)
        if isinstance(widget, QPushButton) or isinstance(widget, QToolButton):
            # Check if the button property has "class" set to "greenBtn"
            icon = widget.icon()
            if icon.isNull():
                continue
            # Check if the button is disabled
            if not widget.isEnabled():
                icon_color = QColor(78,78,78)
            if widget.property('class') == 'greenBtn':
                icon_color = QColor(255,255,255)
            # Change icon color
            # Keep the icon size
            pixmap = icon.pixmap(widget.iconSize(), QIcon.Active, QIcon.On)
            painter = QPainter(pixmap)
            painter.setCompositionMode(QPainter.CompositionMode_SourceIn)
            painter.fillRect(pixmap.rect(), icon_color)
            painter.end()
            widget.setIcon(QIcon(pixmap))