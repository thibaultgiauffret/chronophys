import logging
from logging.handlers import RotatingFileHandler

# Création du logger
logger = logging.getLogger('debug')
logger.setLevel(logging.DEBUG)
fh = RotatingFileHandler('debug.log', mode='a', maxBytes=1*1024*1024, 
                                 backupCount=1, encoding=None, delay=0)
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
logger.addHandler(fh)
logger.addHandler(ch)

def info(msg):
    logger.info(msg)

def debug(msg):
    logger.debug(msg)

def error(msg):
    logger.error(msg)

def warning(msg):
    logger.warning(msg)